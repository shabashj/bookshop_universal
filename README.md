# README #

Online course app - Universal React with Redux, Node js, Express, MongoDB and build a Shopping-cart. It is not my code but I did a lot of debugging by myself to fix all mistakes. 
 
### What is this repository for? ###

Heroku - git commit -a -m �some descript� , git push heroku master, heroku open.
If you need login - heroku login

Mlab - user:testUser pass:asd123
 
Nodemon - Nodemon is a utility that will monitor for any changes in your source and automatically restart your server. 

Express.js - Express is a minimal and flexible Node.js web application framework that provides a robust set of features for web and mobile applications.

Mongoose - Mongoose provides a straight-forward, schema-based solution to model your application data. It includes built-in type casting, validation, query building, business logic hooks and more, out of the box.

Axios - Promise based HTTP client for the browser and node.js

Redux-thunk - Thunk middleware for Redux. Redux Thunk middleware allows you to write action creators that return a function instead of an action. The thunk can be used to delay the dispatch of an action, or to dispatch only if a certain condition is met. The inner function receives the store methods dispatch and getState as parameters.

Http-proxy - node-http-proxy is an HTTP programmable proxying library that supports websockets. It is suitable for implementing components such as reverse proxies and load balancers.

Connect-mongo - MongoDB session store for Connect and Express

Express-session - Simple session middleware for Express


### How do I get set up? ###

Cd bookShop_UNIVERSAL (last one)
Start mongoDB - cd to ~/mongo/bin 
Run - ./mongod --dbpath ~/mongo/dbs
Open robo 3T and connect
Nodemon
Webpack



### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact