"use strict";

import {combineReducers} from 'redux';

// reducers to be combined 
import {bookReducers} from './booksReducers';
import {cartReducers} from './cartReducers';

// COMBINE REDUCERS
export default combineReducers({
    books: bookReducers,
    cart: cartReducers
})