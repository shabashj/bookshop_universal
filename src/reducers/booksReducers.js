"use strict";

// BOOK REDUCERS
export function bookReducers(state = {
    books: []
}, action) {
    switch (action.type) {
        case "GET_BOOKS":
            return {...state, books: [...action.payload]}
            break;
        case "POST_BOOK":
            return { ...state, 
                books: [...state.books, ...action.payload],
                msg: 'Saved! Click to continue',
                style: 'success',
                validation: 'success'
            };
            break;
        case "POST_BOOK_REJECTED":
            return {...state, msg:'Please try again', style:'danger', validation: 'error'};
            break;
         case "RESET_BUTTON":
            return {...state, msg:null, style:'primary', validation: null};
            break;
        case "DELETE_BOOK":
            // create a copy of the current array of books
            const currentBookToDelete = [...state.books];
            // determine at which index in books array is the book to be deleted
            const indexToDelete = currentBookToDelete.findIndex(function (book) {
                return book._id.toString() === action.payload;
            });
            return {
                books: [...currentBookToDelete.slice(0, indexToDelete), ...currentBookToDelete.slice(indexToDelete + 1)]
            };
            break;
        case "UPDATE_BOOK":
            // create a copy of the current array of books
            const currentBookToUpdate = [...state.books];

            // determine at which index in books array is the book to be update
            const indexToUpdate = currentBookToUpdate.findIndex(function (book) {
                return book._id === action.payload._id;
            });
            // Create new book object with the same array of the item we want to replace
            // To achive this we will use ...spread but we could use concat methods too
            const newBookToUpdate = {
                ...currentBookToUpdate[indexToUpdate],
                title: action.payload.title
            }
            // show new book 
            console.log('What is it newBookUpdate', newBookToUpdate);

            return {
                books: [...currentBookToUpdate.slice(0, indexToUpdate),
                    newBookToUpdate, ...currentBookToUpdate.slice(indexToUpdate + 1)]
            };

            break;
    }
    return state;
}